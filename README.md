# dm-tools

A web interface containing several tools useful for DMing tabletop RPGs.


## Setup
```bash
$ /usr/bin/pytyhon3 -m pip install flask
$ /usr/bin/pytyhon3 -m pip install flask-cors
$ /dm-tools-api/runscripts.sh
$ export FLASK_APP=dmtools.py
$ flask run
```

Open /dm-tools-frontend/index.html and refresh to generate a new NPC.