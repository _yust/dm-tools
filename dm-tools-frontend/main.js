

function callAjax(url, callback){
    var xmlhttp;
    xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function(){
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
            callback(xmlhttp.responseText);
        }
    }
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}

function populateNPC(jsonData){
    jsonData = JSON.parse(jsonData);
    npc = jsonData.npc;
    npc.name = toTitleCase(npc.name)
    stats = rollStats();

    var npcName = document.getElementById("npc-name");
    var npcSubTitle = document.getElementById("npc-subtitle");
    var npcAC = document.getElementById("attribute-ac");
    var npcHP = document.getElementById("attribute-hp");
    var npcSpeed = document.getElementById("attribute-speed");
    var npcSTR = document.getElementById("stat-str");
    var npcDEX = document.getElementById("stat-dex");
    var npcCON = document.getElementById("stat-con");
    var npcINT = document.getElementById("stat-int");
    var npcWIS = document.getElementById("stat-wis");
    var npcCHA = document.getElementById("stat-cha");
    var npcAppearance = document.getElementById("appearance");
    var npcTrait = document.getElementById("trait");
    var npcFlaw = document.getElementById("flaw");
    var npcBond = document.getElementById("bond");
    var npcIdeal = document.getElementById("ideal");
    var npcVoice = document.getElementById("voice-description");
    var npcPlotHook = document.getElementById("plot-hook");

    console.log(jsonData);
    npcName.innerHTML = toTitleCase(npc.name);
    npcSubTitle.innerHTML = toTitleCase(npc.gender) + " " + toTitleCase(npc.race) + " " + toTitleCase(npc.profession) + ", " + parseAlignment(npc.alignment);
    npcAppearance.innerHTML = parseAppearance(npc);
    npcTrait.innerHTML = npc.trait;
    npcFlaw.innerHTML = npc.flaw;
    npcBond.innerHTML = npc.bond;
    npcIdeal.innerHTML = npc.ideal;
    npcVoice.innerHTML = parseVoice(npc);
    npcPlotHook.innerHTML = npc.plot_hook;

    npcAC.innerHTML = Math.floor((Math.random() * (20-9)) + 8);
    npcHP.innerHTML = Math.floor((Math.random() * (150-8)) + 7);

    var speeds = [20, 25, 30, 35, 40];
    npcSpeed.innerHTML = speeds[Math.floor(Math.random() * speeds.length)] + "ft";
    npcSTR.innerHTML = stats.str;
    npcDEX.innerHTML = stats.dex;
    npcCON.innerHTML = stats.con;
    npcINT.innerHTML = stats.int;
    npcWIS.innerHTML = stats.wis;
    npcCHA.innerHTML = stats.cha;

    document.getElementById('site-content-container').style.visibility = "visible";
    
}

function parseAlignment(alignment){
    switch(alignment.toLowerCase()){
        case "lg":
            return "Lawful Good";
        case "ng":
            return "Neutral Good";
        case "cg":
            return "Chaotic Good";
        case "ln":
            return "Lawful Neutral";
        case "n":
            return "Neutral";
        case "cn":
            return "Chaotic Neutral";
        case "le":
            return "Lawful Evil";
        case "ne":
            return "Neutral Evil";
        case "ce":
            return "Chaotic Evil";
    }
}

function rollStats(){
    function d6(){
        return Math.floor((Math.random() * 6) + 1);
    }

    function statRoll(){
        return d6() + d6() + d6();
    }

    mod = {
        1: "-5",
        2: "-4",
        3: "-4",
        4: "-3",
        5: "-3",
        6: "-2",
        7: "-2",
        8: "-1",
        9: "-1",
        10: "0",
        11: "0",
        12: "+1",
        13: "+1",
        14: "+2",
        15: "+2",
        16: "+3",
        17: "+3",
        18: "+4"
    };

    stats = {
        str: '',
        dex: '',
        con: '',
        int: '',
        wis: '',
        cha: ''
    };

    var str = statRoll();
    var dex = statRoll();
    var con = statRoll();
    var int = statRoll();
    var wis = statRoll();
    var cha = statRoll();
    stats.str = str + " (" + mod[str] + ")";
    stats.dex = dex + " (" + mod[dex] + ")";
    stats.con = con + " (" + mod[con] + ")";
    stats.int = int + " (" + mod[int] + ")";
    stats.wis = wis + " (" + mod[wis] + ")";
    stats.cha = cha + " (" + mod[cha] + ")";

    return stats;
}

function parseVoice(npc){
    return toTitleCase(this.npc.name) + " has a " + this.npc.voice_pitch + " pitched " + this.npc.voice_speed + " voice that is " + this.npc.vocal_texture + ". " +
            "They speak " + this.npc.vocal_pattern + ", and they " + this.npc.mannerism + " while they're speaking.";

}

function parseAppearance(npc){

    var humanoids = ["aasimar", "drow", "dwarf", "elf", "firlbolg", "genasi", "gith", "goliath", "gnome", "half-elf", "halfling", "half-orc", "human", "tiefling", "triton"];
    var lizardlikes = ["dragonborn", "kobold", "lizardfolk", "tortle"];
    var orclikes = ["bugbear", "goblin", "hobgoblin", "orc"];
    var birds = ["aarakocra", "kenku"];
    var cats = ["tabaxi"];

    var description_string = ''; 

    if (humanoids.includes(npc.race)){
        console.log("NPC Race in humanoids");
        if(npc.gender == "male") {
            description_string = npc.name + " is a " + npc.weight + " " + npc.race + " of " + npc.height + " height." +
                                "He has " + npc.hair_length + " " + npc.hair_color + " " + npc.hair_type + " hair accenting his " +
                                npc.face_shape + " face that is decorated with a " + npc.facial_hair + ". He has " + npc.eye_shape +
                                " " + npc.eye_color + " eyes, " + npc.ear + " ears, and a " + npc.nose + " nose. His skin tone is " + npc.skin_tone +
                                ", and he is wearing clothes that are " + npc.clothing + ". He smells just like " + npc.smell + ".";
        } else {
            if (npc.race == "dwarf"){
                description_string = npc.name + " is a " + npc.weight + " " + npc.race + " of " + npc.height + " height." +
                                    "She has " + npc.hair_length + " " + npc.hair_color + " " + npc.hair_type + " hair accenting her " +
                                    npc.face_shape + " face that is decorated with a " + npc.facial_hair + ". She has " + npc.eye_shape +
                                    " " + npc.eye_color + " eyes, " + npc.ear + " ears, and a " + npc.nose + " nose. Her skin tone is " + npc.skin_tone +
                                    ", and she is wearing clothes that are " + npc.clothing + ". She smells just like " + npc.smell + ".";
            } else {
                description_string = npc.name + " is a " + npc.weight + " " + npc.race + " of " + npc.height + " height." +
                                    "She has " + npc.hair_length + " " + npc.hair_color + " " + npc.hair_type + " hair accenting her " +
                                    npc.face_shape + " face. She has " + npc.eye_shape +
                                    " " + npc.eye_color + " eyes, " + npc.ear + " ears, and a " + npc.nose + " nose. Her skin tone is " + npc.skin_tone +
                                    ", and she is wearing clothes that are " + npc.clothing + ". She smells just like " + npc.smell + ".";
            }
        }
    }

    if (lizardlikes.includes(npc.race)){
        console.log("NPC Race in lizardlikes");
        if(npc.gender == "male") {
            description_string = npc.name + " is a " + npc.weight + " " + npc.race + " of " + npc.height + " height. " + 
                                "He has " + npc.skin_tone + " scales, a " + npc.face_shape + " face, and a " + npc.nose + " nose. " +
                                "His " + npc.eye_color + " " + npc.eye_shape + " eyes stare back at you as you get a wiff of " + npc.smell + " from them." +
                                "He is wearing clothes that are " + npc.clothing + ".";
        } else {
            description_string = npc.name + " is a " + npc.weight + " " + npc.race + " of " + npc.height + " height. " + 
                                "She has " + npc.skin_tone + " scales, a " + npc.face_shape + " face, and a " + npc.nose + " nose. " +
                                "Her " + npc.eye_color + " " + npc.eye_shape + " eyes stare back at you as you get a wiff of " + npc.smell + " from them." +
                                "She is wearing clothes that are " + npc.clothing + ".";
        }
    }

    if (orclikes.includes(npc.race)) {
        console.log("NPC Race in orc");
        if(npc.gender == "male") {
            description_string = npc.name + " is a " + npc.weight + " " + npc.race + " of " + npc.height + " height." +
                                "He has " + npc.hair_length + " " + npc.hair_color + " " + npc.hair_type + " hair accenting his " +
                                npc.face_shape + " face that is decorated with a " + npc.facial_hair + ". He has " + npc.eye_shape +
                                " " + npc.eye_color + " eyes, " + npc.ear + " ears, and a " + npc.nose + " nose. His skin tone is " + npc.skin_tone +
                                ", and he is wearing clothes that are " + npc.clothing + ". He smells just like " + npc.smell + ".";
        } else {
            description_string = npc.name + " is a " + npc.weight + " " + npc.race + " of " + npc.height + " height." +
                                "She has " + npc.hair_length + " " + npc.hair_color + " " + npc.hair_type + " hair accenting her " +
                                npc.face_shape + " face. She has " + npc.eye_shape +
                                " " + npc.eye_color + " eyes, " + npc.ear + " ears, and a " + npc.nose + " nose. Her skin tone is " + npc.skin_tone +
                                ", and she is wearing clothes that are " + npc.clothing + ". She smells just like " + npc.smell + ".";
        }
    }

    if (birds.includes(npc.race)){
        console.log("NPC Race in bird");
        description_string = npc.name + " is a " + npc.weight + " " + npc.race + " of " + npc.height + " height. " + 
                            "They have " + npc.skin_tone + " plumage, a " + npc.face_shape + " face, and a " + npc.nose + " beak. " +
                            "Their " + npc.eye_color + " " + npc.eye_shape + " eyes stare back at you as you get a wiff of " + npc.smell + " from them. " +
                            "They are wearing clothes that are " + npc.clothing + ".";
    }

    if (cats.includes(npc.race)){
        console.log("NPC Race in cat");
        description_string = npc.name + " is a " + npc.weight + " " + npc.race + " of " + npc.height + " height. " + 
                            "They have " + npc.skin_tone + " fur, a " + npc.face_shape + " face, and a " + npc.nose + " nose. " +
                            "Their " + npc.eye_color + " " + npc.eye_shape + " eyes stare back at you as you get a wiff of " + npc.smell + " from them. " +
                            "They are wearing clothes that are " + npc.clothing + ".";
    }

    console.log("Description string: " + description_string);
    return description_string;
}


function toTitleCase(str) {
    return str.replace(
        /\w*/g,
        function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        }
    );
}

callAjax("http://api.goblinknifefight.com/npc", populateNPC);

