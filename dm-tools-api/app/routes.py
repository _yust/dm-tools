from app import app
import sqlite3
import json
from flask import abort

@app.route('/')
@app.route('/index')
def index():
    return 'Hello world!'

@app.route('/npc')
def return_npc():
    return json.dumps(generate_npc())

@app.route('/npc/race=<string:race>')
def return_npc_race(race):
    return json.dumps(generate_npc(race=race.lower()))

@app.route('/npc/profession=<string:profession>')
def return_npc_profession(profession):
    return json.dumps(generate_npc(profession=profession.lower()))

@app.route('/npc/alignment=<string:alignment>')
def return_npc_alignment(alignment):
    return json.dumps(generate_npc(alignment=alignment.upper()))
    
def generate_npc(race=None, alignment=None, profession=None):
    db = sqlite3.connect("npc_db")
    cur = db.cursor()

    npc = {
        'npc': {
            'name':             '',
            'race':             '',
            'alignment':        '',
            'gender':           '',
            'profession':       '',
            'hair_type':        '',
            'facial_hair':      '',
            'hair_color':       '',
            'hair_length':      '',
            'face_shape':       '',
            'eye_color':        '',
            'eye_shape':        '',
            'ears':             '',
            'nose':             '',
            'skin_tone':        '',
            'smell':            '',
            'weight':           '',
            'height':           '',
            'voice_speed':      '',
            'voice_pitch':      '',
            'vocal_pattern':    '',
            'vocal_texture':    '',
            'mannerism':        '',
            'plot_hook':        '',
            'clothing':         '',
            'bond':             '',
            'trait':            '',
            'flaw':             '',
            'ideal':            ''
        }
    }


    
    #race
    if race is None:
        cur.execute("SELECT race FROM npc_race ORDER BY RANDOM() LIMIT 1;")
        npc["npc"]["race"] = cur.fetchone()[0]
    else:
        cur.execute("SELECT race FROM npc_race WHERE race = ?;", (race,))
        test_race = cur.fetchone()
        if test_race is None:
            return abort(404)
        else:
            npc["npc"]["race"] = race

    print(npc["npc"]["race"])

    #gender
    cur.execute("SELECT gender FROM npc_gender ORDER BY RANDOM() LIMIT 1;")
    npc["npc"]["gender"] = cur.fetchone()[0]

    #name
    cur.execute("SELECT name FROM npc_name WHERE race = ? AND gender = ? ORDER BY RANDOM() LIMIT 1;", (npc["npc"]["race"], npc["npc"]["gender"]))
    npc["npc"]["name"] = cur.fetchone()[0]

    #surname
    if npc["npc"]["race"] in ["dwarf", "tortle", "bugbear"]:
        cur.execute("SELECT surname FROM npc_surname WHERE race = ? ORDER BY RANDOM() LIMIT 1;", (npc["npc"]["race"],))
        npc["npc"]["name"] += ' ' + cur.fetchone()[0]

    #alignment
    if alignment is None:
        cur.execute("SELECT alignment FROM npc_alignment ORDER BY RANDOM() LIMIT 1;")
        npc["npc"]["alignment"] = cur.fetchone()[0]
    else:
        cur.execute("SELECT alignment FROM npc_alignment WHERE alignment = ?;", (alignment,))
        test_alignment = cur.fetchone()
        if test_alignment is None:
            return abort(404)
        else:
            npc["npc"]["alignment"] = alignment

    #profession
    if profession is None:
        cur.execute("SELECT profession FROM npc_profession ORDER BY RANDOM() LIMIT 1;")
        npc["npc"]["profession"] = cur.fetchone()[0]
    else:
        cur.execute("SELECT profession FROM npc_profession WHERE profession = ?;", (profession,))
        test_profession = cur.fetchone()
        if test_profession is None:
            return abort(404)
        else:
            npc["npc"]["profession"] = profession

    #hair
    #hair length
    cur.execute("SELECT hair_length FROM npc_hair_length ORDER BY RANDOM() LIMIT 1;")
    npc["npc"]["hair_length"] = cur.fetchone()[0]

    #hair type
    cur.execute("SELECT hair_type FROM npc_hair_type WHERE race = ? ORDER BY RANDOM() LIMIT 1;", (npc["npc"]["race"],))
    npc["npc"]["hair_type"] = cur.fetchone()[0]

    #hair color
    cur.execute("SELECT hair_color FROM npc_hair_color WHERE race = ? ORDER BY RANDOM() LIMIT 1;", (npc["npc"]["race"],))
    npc["npc"]["hair_color"] = cur.fetchone()[0]

    #facial hair
    if npc["npc"]["gender"] == "female" and npc["npc"]["race"] != "dwarf":
        npc["npc"]["facial_hair"] = 'no facial hair'
    else:
        cur.execute("SELECT facial_hair FROM npc_facial_hair WHERE race = ? ORDER BY RANDOM() LIMIT 1;", (npc["npc"]["race"],))
        npc["npc"]["facial_hair"] = cur.fetchone()[0]

    #face shape
    cur.execute("SELECT face_shape FROM npc_face_shape ORDER BY RANDOM() LIMIT 1;")
    npc["npc"]["face_shape"] = cur.fetchone()[0]

    #eye
    #eye color
    cur.execute("SELECT eye_color FROM npc_eye_color ORDER BY RANDOM() LIMIT 1;")
    npc["npc"]["eye_color"] = cur.fetchone()[0]

    #eye shape
    cur.execute("SELECT eye_shape FROM npc_eye_shape ORDER BY RANDOM() LIMIT 1;")
    npc["npc"]["eye_shape"] = cur.fetchone()[0]

    #ears
    cur.execute("SELECT ear FROM npc_ear WHERE race = ? ORDER BY RANDOM() LIMIT 1;", (npc["npc"]["race"],))
    npc["npc"]["ears"] = cur.fetchone()[0]

    #nose
    cur.execute("SELECT nose FROM npc_nose WHERE race = ? ORDER BY RANDOM() LIMIT 1;", (npc["npc"]["race"],))
    npc["npc"]["nose"] = cur.fetchone()[0]
    
    #skin tone
    cur.execute("SELECT skin_tone FROM npc_skin_tone WHERE race = ? ORDER BY RANDOM() LIMIT 1;", (npc["npc"]["race"],))
    npc["npc"]["skin_tone"] = cur.fetchone()[0]

    #smell
    cur.execute("SELECT smell FROM npc_smell ORDER BY RANDOM() LIMIT 1;")
    npc["npc"]["smell"] = cur.fetchone()[0]

    #weight
    cur.execute("SELECT weight FROM npc_weight ORDER BY RANDOM() LIMIT 1;")
    npc["npc"]["weight"] = cur.fetchone()[0]

    #height
    cur.execute("SELECT height FROM npc_height ORDER BY RANDOM() LIMIT 1;")
    npc["npc"]["height"] = cur.fetchone()[0]

    #voice speed
    cur.execute("SELECT voice_speed FROM npc_voice_speed ORDER BY RANDOM() LIMIT 1;")
    npc["npc"]["voice_speed"] = cur.fetchone()[0]

    #voice pitch
    cur.execute("SELECT voice_pitch FROM npc_voice_pitch ORDER BY RANDOM() LIMIT 1;")
    npc["npc"]["voice_pitch"] = cur.fetchone()[0]

    #voice pattern
    cur.execute("SELECT vocal_pattern FROM npc_voice_vocal_pattern ORDER BY RANDOM() LIMIT 1;")
    npc["npc"]["vocal_pattern"] = cur.fetchone()[0]

    #voice texture
    cur.execute("SELECT vocal_texture FROM npc_voice_vocal_texture ORDER BY RANDOM() LIMIT 1;")
    npc["npc"]["vocal_texture"] = cur.fetchone()[0]

    #mannerism
    cur.execute("SELECT voice_mannerism FROM npc_voice_mannerism ORDER BY RANDOM() LIMIT 1;")
    npc["npc"]["mannerism"] = cur.fetchone()[0]

    #plot hook
    cur.execute("SELECT plot_hook FROM npc_plot_hook ORDER BY RANDOM() LIMIT 1;")
    npc["npc"]["plot_hook"] = cur.fetchone()[0]

    #clothing
    cur.execute("SELECT clothing FROM npc_clothing ORDER BY RANDOM() LIMIT 1;")
    npc["npc"]["clothing"] = cur.fetchone()[0]

    #bond
    cur.execute("SELECT bond FROM npc_personality_bond ORDER BY RANDOM() LIMIT 1;")
    npc["npc"]["bond"] = cur.fetchone()[0]

    #trait
    cur.execute("SELECT trait FROM npc_personality_trait ORDER BY RANDOM() LIMIT 1;")
    npc["npc"]["trait"] = cur.fetchone()[0]

    #flaw
    cur.execute("SELECT flaw FROM npc_personality_flaw ORDER BY RANDOM() LIMIT 1;")
    npc["npc"]["flaw"] = cur.fetchone()[0]

    #ideal
    cur.execute("SELECT ideal FROM npc_personality_ideal ORDER BY RANDOM() LIMIT 1;")
    npc["npc"]["ideal"] = cur.fetchone()[0]

    return npc

def generate_shop():
    pass

def generate_market():
    pass