import sqlite3

npc_db = sqlite3.connect("../npc_db")
cur = npc_db.cursor()

genders = ["male", "female"]

for gender in genders:
    cur.execute("INSERT INTO npc_gender (gender) VALUES (?);", (gender,))
    npc_db.commit()