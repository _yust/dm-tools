import sqlite3

npc_db = sqlite3.connect("../npc_db")
cur = npc_db.cursor()

data = {
    "aarakocra":    ["short", "stubbed", "duck-like", "pointed"],
    "aasimar":      ["crooked", "bulbous", "button", "narrow", "long", "broad", "angular", "broken", "hawk-like", "wide", "delicate"],
    "bugbear":      ["bear-like"],
    "dragonborn":   ["double-slit"],
    "drow":         ["crooked", "bulbous", "button", "narrow", "long", "broad", "angular", "broken", "hawk-like", "wide", "delicate"],
    "dwarf":        ["crooked", "bulbous", "button", "narrow", "long", "broad", "angular", "broken", "hawk-like", "wide", "delicate"],
    "elf":          ["crooked", "bulbous", "button", "narrow", "long", "broad", "angular", "broken", "hawk-like", "wide", "delicate"],
    "firlbolg":     ["crooked", "bulbous", "button", "narrow", "long", "broad", "angular", "broken", "hawk-like", "wide", "delicate"],  
    "genasi":       ["crooked", "bulbous", "button", "narrow", "long", "broad", "angular", "broken", "hawk-like", "wide", "delicate"],
    "gith":         ["double-slit"],
    "goblin":       ["crooked", "bulbous", "button", "narrow", "long", "broad", "angular", "broken", "hawk-like", "wide", "delicate"],
    "goliath":      ["crooked", "bulbous", "button", "narrow", "long", "broad", "angular", "broken", "hawk-like", "wide", "delicate"],
    "gnome":        ["crooked", "bulbous", "button", "narrow", "long", "broad", "angular", "broken", "hawk-like", "wide", "delicate"],
    "half-elf":     ["crooked", "bulbous", "button", "narrow", "long", "broad", "angular", "broken", "hawk-like", "wide", "delicate"],  
    "halfling":     ["crooked", "bulbous", "button", "narrow", "long", "broad", "angular", "broken", "hawk-like", "wide", "delicate"],  
    "half-orc":     ["crooked", "bulbous", "button", "narrow", "long", "broad", "angular", "broken", "hawk-like", "wide", "delicate"],
    "hobgoblin":    ["crooked", "bulbous", "button", "narrow", "long", "broad", "angular", "broken", "hawk-like", "wide", "delicate"],   
    "human":        ["crooked", "bulbous", "button", "narrow", "long", "broad", "angular", "broken", "hawk-like", "wide", "delicate"],
    "kenku":        ["short", "stubbed", "duck-like", "pointed"],
    "kobold":       ["lizard-like"],
    "lizardfolk":   ["lizard-like"],
    "orc":          ["crooked", "bulbous", "button", "narrow", "long", "broad", "angular", "broken", "hawk-like", "wide", "delicate"],
    "tabaxi":       ["cat-like"],
    "tiefling":     ["crooked", "bulbous", "button", "narrow", "long", "broad", "angular", "broken", "hawk-like", "wide", "delicate"],  
    "tortle":       ["double-slit"],
    "triton":       ["crooked", "bulbous", "button", "narrow", "long", "broad", "angular", "broken", "hawk-like", "wide", "delicate"]
}

for race in data:
    for nose in data[race]:
        cur.execute("INSERT INTO npc_nose (race, nose) VALUES (?, ?);", (race, nose))
        npc_db.commit()