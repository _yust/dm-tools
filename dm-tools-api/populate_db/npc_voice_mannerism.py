import sqlite3

npc_db = sqlite3.connect("../npc_db")
cur = npc_db.cursor()

mannerisms = [
    "taps chin",
    "cracks knuckles",
    "taps feet",
    "pulls on ear",
    "clicks tongue",
    "bites nails",
    "swallows frequently",
    "hands in pockets",
    "plays with a coin",
    "stroked chin/beard",
    "plays with hair",
    "never makes eye contact",
    "snaps often",
    "shuffles feet",
    "writes down everything",
    "is a close-talker",
    "winks when it doesn't actually allude to anything",
    "rests hand on weapon (if they have one)",
    "never blinks",
    "drums fingers",
    "spits tobacco",
    "licks lips"
]

for mannerism in mannerisms:
    cur.execute("INSERT INTO npc_voice_mannerism (voice_mannerism) VALUES (?);", (mannerism,))
    npc_db.commit()