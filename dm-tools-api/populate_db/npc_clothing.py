import sqlite3

npc_db = sqlite3.connect("../npc_db")
cur = npc_db.cursor()

clothing_list = [
                "crisp and new",
                "fashionable and hip",
                "a bit old-fashioned",
                "of the highest quality",
                "faded, but in good condition",
                "faded and patched",
                "torn in places and missing buttons",
                "tattered and worn"
            ]

for clothing in clothing_list:
    cur.execute("INSERT INTO npc_clothing VALUES (?);", (clothing,))
    npc_db.commit()