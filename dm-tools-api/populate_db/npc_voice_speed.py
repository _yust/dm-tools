import sqlite3

npc_db = sqlite3.connect("../npc_db")
cur = npc_db.cursor()

speeds = [
    "slow",
    "medium",
    "fast"
]

for speed in speeds:
    cur.execute("INSERT INTO npc_voice_speed (voice_speed) VALUES (?);", (speed,))
    npc_db.commit()