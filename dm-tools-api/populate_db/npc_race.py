import sqlite3

npc_db = sqlite3.connect("../npc_db")
cur = npc_db.cursor()

races = [
    "aarakocra",
    "aasimar",
    "bugbear",
    "dragonborn",
    "drow",
    "dwarf",
    "elf",
    "firlbolg",  
    "genasi",
    "gith",
    "goblin",
    "goliath",
    "gnome",
    "half-elf",  
    "halfling",  
    "half-orc",
    "hobgoblin",   
    "human",
    "kenku",
    "kobold",
    "lizardfolk",
    "orc",
    "tabaxi",
    "tiefling",  
    "tortle",
    "triton"
]

for race in races:
    cur.execute("INSERT INTO npc_race (race) VALUES (?);", (race,))
    npc_db.commit()