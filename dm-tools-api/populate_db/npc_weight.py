import sqlite3

npc_db = sqlite3.connect("../npc_db")
cur = npc_db.cursor()

weights = ["gaunt", "scrawny", "skinny", "weak", "average", "strong", "bulky", "muscular", "obese", "chubby", "athletic", "absolute unit"]

for weight in weights:
    cur.execute("INSERT INTO npc_weight (weight) VALUES (?);", (weight,))
    npc_db.commit()