import sqlite3

npc_db = sqlite3.connect("../npc_db")
cur = npc_db.cursor()

data = {
    "aarakocra":    ["hidden behind feathers"],
    "aasimar":      ["diamond", "rectangular", "round", "large", "small"],
    "bugbear":      ["diamond", "rectangular", "round", "large", "small"],
    "dragonborn":   ["slit"],
    "drow":         ["diamond", "rectangular", "round", "large", "small"],
    "dwarf":        ["diamond", "rectangular", "round", "large", "small"],
    "elf":          ["long and pointed", "short and pointed"],
    "firlbolg":     ["diamond", "rectangular", "round", "large", "small"],  
    "genasi":       ["diamond", "rectangular", "round", "large", "small"],
    "gith":         ["pointed"],
    "goblin":       ["long and pointed", "short and pointed"],
    "goliath":      ["diamond", "rectangular", "round", "large", "small"],
    "gnome":        ["diamond", "rectangular", "round", "large", "small"],
    "half-elf":     ["long and pointed", "short and pointed", "diamond", "rectangular", "round", "large", "small"],  
    "halfling":     ["diamond", "rectangular", "round", "large", "small"],  
    "half-orc":     ["diamond", "rectangular", "round", "large", "small"],
    "hobgoblin":    ["long and pointed", "short and pointed"],   
    "human":        ["diamond", "rectangular", "round", "large", "small"],
    "kenku":        ["hidden behind feathers"],
    "kobold":       ["long and pointed", "short and pointed"],
    "lizardfolk":   ["slit"],
    "orc":          ["diamond", "rectangular", "round", "large", "small"],
    "tabaxi":       ["triangular", "rounded", "folded-back triangular", "folded back round"],
    "tiefling":     ["curled", "ram-like", "pointed out", "straight up", "twisted outwards", "straight out"],  
    "tortle":       ["slit"],
    "triton":       ["diamond", "rectangular", "round", "large", "small"]
}

for race in data:
    for ear in data[race]:
        cur.execute("INSERT INTO npc_ear (race, ear) VALUES (?, ?);", (race, ear))
        npc_db.commit()