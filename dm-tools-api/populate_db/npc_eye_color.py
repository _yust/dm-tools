import sqlite3

npc_db = sqlite3.connect("../npc_db")
cur = npc_db.cursor()

eye_colors = [
            "amber",
            "golden",
            "blue",
            "turqoise",
            "green",
            "hazel",
            "pitch black",
            "red",
            "brown",
            "aquamarine",
            "chestnut brown",
            "chocolate brown",
            "golden brown",
            "honey brown",
            "walnut brown",
            "gray",
            "emerald",
            "violet",
            "earth-tone"
            ]

for eye_color in eye_colors:
    cur.execute("INSERT INTO npc_eye_color (eye_color) VALUES (?);", (eye_color,))
    npc_db.commit()