import sqlite3

npc_db = sqlite3.connect("../npc_db")
cur = npc_db.cursor()

vocal_textures = [
    "gruff",
    "smooth",
    "strained",
    "relaxed",
    "breathy",
    "from the back of the throat",
    "scratchy",
    "nasally"
]

for vocal_texture in vocal_textures:
    cur.execute("INSERT INTO npc_voice_vocal_texture (vocal_texture) VALUES (?);", (vocal_texture,))
    npc_db.commit()