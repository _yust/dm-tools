import sqlite3

npc_db = sqlite3.connect("../npc_db")
cur = npc_db.cursor()

alignments = ["LG", "NG", "CG", "LN", "N", "CN", "LE", "NE", "CE"]

for alignment in alignments:
    cur.execute("INSERT INTO npc_alignment (alignment) VALUES (?);", (alignment,))
    npc_db.commit()