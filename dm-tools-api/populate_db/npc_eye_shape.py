import sqlite3

npc_db = sqlite3.connect("../npc_db")
cur = npc_db.cursor()

eye_shapes = [
    "sleepy",
    "shifty",
    "watery",
    "bright",
    "cold",
    "smiling",
    "close-set",
    "wild",
    "distant",
    "piercing",
    "watchful",
    "dark",
    "slightly-crossed",
    "wide",
    "beautiful",
    "beady",
    "penetrating",
    "deep",
    "almond",
    "sunken",
    "cat-like",
    "squinty",
    "narrow",
    "deep-set",
    "bulging"
]

for eye_shape in eye_shapes:
    cur.execute("INSERT INTO npc_eye_shape (eye_shape) VALUES (?);", (eye_shape,))
    npc_db.commit()