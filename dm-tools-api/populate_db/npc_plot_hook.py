import sqlite3

npc_db = sqlite3.connect("../npc_db")
cur = npc_db.cursor()

plot_hooks = [
    "Just witnessed a brutal murder.",
    "Claims to be an exiled member of a secret organization.",
    "They are willing to show you where the treasure is located, for a cut.",
    "Is the head of a local drug dealing cartel.",
    "Part of a secret organization that wants to overthrow the local government.",
    "Recently came upon an extravagant amount of fresh fish, and needs to get rid of it before it spoils.",
    "Is in dire need of demon blood",
    "Recently lost everything in a bad gamble. Is willing to work for anything.",
    "Needs you to deliver a tamed demon to a nearby circus.",
    "Claims their home is infested with zombies. Upon further investigation, the zombies used to live there and were previously killed by this NPC.",
    "Needs help taming a lycanthrope.",
    "Has recently obtained a map to a great treasure, but is not an adventurer.",
    "Believes that a beholder lives under their house.",
    "Obsessed with a nearby tavern that has incredible ale.",
    "Challenges a PC to a duel to the death.",
    "Is secretly a vampire.",
    "Is followed by an entourage of wild animals at all times.",
    "Believes there is a voice in thier head that narrates the world around them.",
    "Started using snakes as currency, and its actually starting to catch on.",
    "Is followed by a talking cat.",
    "Is currently chasing a bandit, and asks the PCs for help.",
    "Has been hired to assassinate the PCs.",
    "Has been hired to steal money from the PCs.",
    "Is a friendly vampire, and is trying to sell the PCs on the idea of also becoming vampires.",
    "Is obsessed with a nearby grove with strange plant-life.",
    "Needs help exorcising their brother.",
    "Has fallen in love with a fairy from the nearby woods.",
    "Gives the PCs a magical coin. Turns out, the coin was stolen from a wizard who is hellbent on recovering it.",
    "Claims they are being stalked by a mindflayer.",
    "Has a duel to the death tomorrow, but is afraid they will lose.",
    "Is looking for their lost daughter.",
    "Is a cannibal, and preaches how great cannibalism is.",
    "Placed a bet on an upcoming fighting competition, but needs help rigging it.",
    "Keeps a fairy locked up in their basement.",
    "Is being chased by law enforcement and asks the PCs for protection.",
    "Is being controlled by an evil spirit during the day, and is fully aware. They only have control of themselves during the night.",
    "'Accidentally' summoned a demon while 'practicing' magic.",
    "Secretly carries around a black dragon egg.",
    "Has been cursed into speaking only gibberish.",
    "Is being haunted by the ghost of their dead daughter.",
    "Recently woke up with a magical rune on their chest.",
    "Is slowly becoming a plant.",
    "Believes halflings are sneaking into their house and stealing their food.",
    "Is looking for their lost childhood friend.",
    "Has fallen in love with a demon and doesn't know what to do.",
    "Discovered a fountain of wild magic in their basement, and can't control it."
]

for plot_hook in plot_hooks:
    cur.execute("INSERT INTO npc_plot_hook (plot_hook) VALUES (?);", (plot_hook,))
    npc_db.commit()