import sqlite3

npc_db = sqlite3.connect("../npc_db")
cur = npc_db.cursor()

data = {
    "aarakocra":    ["none"],
    "aasimar":      ["red", "blonde", "brown", "black", "silver"],
    "bugbear":      ["brown", "red"],
    "dragonborn":   ["none"],
    "drow":         ["white"],
    "dwarf":        ["black", "brown", "red"],
    "elf":          ["dark brown", "autumn orange", "mossy green", "deep gold"],
    "firlbolg":     ["brown", "blonde", "red"],  
    "genasi":       ["light blonde", "golden blonde", "brown", "red", "black"],
    "gith":         ["brown"],
    "goblin":       ["black"],
    "goliath":      ["black", "brown"],
    "gnome":        ["light blonde", "golden blonde", "brown", "red", "black"],
    "half-elf":     ["light blonde", "golden blonde", "brown", "red", "black", "dark brown", "autumn orange", "mossy green", "deep gold"],  
    "halfling":     ["light blonde", "golden blonde", "brown", "red", "black"],  
    "half-orc":     ["light blonde", "golden blonde", "brown", "red", "black"],
    "hobgoblin":    ["dark brown", "dark gray", "orange", "red"],   
    "human":        ["light blonde", "golden blonde", "brown", "red", "black"],
    "kenku":        ["none"],
    "kobold":       ["none"],
    "lizardfolk":   ["none"],
    "orc":          ["black"],
    "tabaxi":       ["none"],
    "tiefling":     ["red", "brown", "black", "dark blue", "purple"],  
    "tortle":       ["none"],
    "triton":       ["turquiose", "green", "deep blue", "light green", "teal"]
}

for race in data:
    for hair_color in data[race]:
        cur.execute("INSERT INTO npc_hair_color (race, hair_color) VALUES (?, ?);", (race, hair_color))
        npc_db.commit()