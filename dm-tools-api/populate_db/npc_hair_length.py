import sqlite3

npc_db = sqlite3.connect("../npc_db")
cur = npc_db.cursor()

hair_lengths = ["short", "medium", "long"]

for hair_length in hair_lengths:
    cur.execute("INSERT INTO npc_hair_length (hair_length) VALUES (?);", (hair_length,))
    npc_db.commit()