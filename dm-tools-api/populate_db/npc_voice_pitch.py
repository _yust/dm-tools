import sqlite3

npc_db = sqlite3.connect("../npc_db")
cur = npc_db.cursor()

pitches = [
    "low",
    "medium",
    "high"
]

for pitch in pitches:
    cur.execute("INSERT INTO npc_voice_pitch (voice_pitch) VALUES (?);", (pitch,))
    npc_db.commit()