import sqlite3

npc_db = sqlite3.connect("../npc_db")
cur = npc_db.cursor()

heights = ["extra short", "short", "shorter than average", "average", "taller than average", "tall", "extra tall"]

for height in heights:
    cur.execute("INSERT INTO npc_height (height) VALUES (?);", (height,))
    npc_db.commit()