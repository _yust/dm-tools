import sqlite3

npc_db = sqlite3.connect("../npc_db")
cur = npc_db.cursor()

data = {
    "aarakocra":    ["red", "orange", "yellow", "brown", "gray"],
    "aasimar":      ["tan", "fair", "peach", "coffee", "gray", "olive"],
    "bugbear":      ["amber", "clay", "tan", "brown", "yellow"],
    "dragonborn":   ["scarlet", "gold", "rust", "ocher", "bronze", "brown"],
    "drow":         ["black", "dark blue", "gray", "violet"],
    "dwarf":        ["tan", "fair", "peach", "coffee", "gray", "olive"],
    "elf":          ["tan", "coffee", "gray", "olive", "dark brown", "light brown"],
    "firlbolg":     ["sky blue", "navy", "midnight blue", "ocean blue"],  
    "genasi":       ["red", "green", "blue", "gray"],
    "gith":         ["green", "yellow", "orange", "brown"],
    "goblin":       ["amber", "clay", "tan", "brown", "yellow", "red"],
    "goliath":      ["gray", "brown"],
    "gnome":        ["tan", "fair", "coffee", "gray", "olive"],
    "half-elf":     ["tan", "fair", "peach", "coffee", "olive"],  
    "halfling":     ["tan", "fair", "peach", "coffee", "olive"],  
    "half-orc":     ["light green", "earth green", "forest green", "purple-gray"],
    "hobgoblin":    ["orange", "clay", "amber", "goldenrod", "brown"],   
    "human":        ["tan", "fair", "peach", "coffee", "olive"],
    "kenku":        ["black", "brown"],
    "kobold":       ["reddish-brown"],
    "lizardfolk":   ["green", "black", "gray", "brown"],
    "orc":          ["light gray", "gray", "dark gray", "light green", "earth green", "forest green"],
    "tabaxi":       ["brown red", "black spotted white", "light yellow", "amber", "black", "brown", "red", "orange", "orange with white stripes", "amber with white striped"],
    "tiefling":     ["tan", "fair", "peach", "coffee", "olive", "red", "light red", "dark red", "light purple", "pink"],  
    "tortle":       ["olive", "turquoise", "forest green"],
    "triton":       ["silver", "silver-blue"]
}

for race in data:
    for skin_tone in data[race]:
        cur.execute("INSERT INTO npc_skin_tone (race, skin_tone) VALUES (?, ?);", (race, skin_tone))
        npc_db.commit()