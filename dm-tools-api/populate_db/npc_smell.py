import sqlite3

npc_db = sqlite3.connect("../npc_db")
cur = npc_db.cursor()

smells = [
    "fresh cut timber",
    "rain",
    "smoke",
    "coffee",
    "roses",
    "chocolate",
    "campfire",
    "lavender",
    "leather",
    "the ocean",
    "perfume",
    "roasted chicken",
    "garlic",
    "spices",
    "sweat",
    "manure",
    "dirt",
    "vomit",
    "sewage",
    "tallow"
]

for smell in smells:
    cur.execute("INSERT INTO npc_smell (smell) VALUES (?);", (smell,))
    npc_db.commit()