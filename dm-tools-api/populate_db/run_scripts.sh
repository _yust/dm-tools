#!/bin/bash

python3 create_db.py

python3 npc_alignment.py
python3 npc_clothing.py
python3 npc_ear.py
python3 npc_eye_color.py
python3 npc_eye_shape.py
python3 npc_face_shape.py
python3 npc_facial_hair.py
python3 npc_gender.py
python3 npc_hair_color.py
python3 npc_hair_length.py
python3 npc_hair_type.py
python3 npc_height.py
python3 npc_name.py
python3 npc_nose.py
python3 npc_personality_bond.py
python3 npc_personality_flaw.py
python3 npc_personality_ideal.py
python3 npc_personality_trait.py
python3 npc_plot_hook.py
python3 npc_profession.py
python3 npc_race.py
python3 npc_skin_tone.py
python3 npc_smell.py
python3 npc_surname.py
python3 npc_voice_mannerism.py
python3 npc_voice_pitch.py
python3 npc_voice_speed.py
python3 npc_voice_vocal_pattern.py
python3 npc_voice_vocal_texture.py
python3 npc_weight.py