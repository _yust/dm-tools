import sqlite3

npc_db = sqlite3.connect("../npc_db")
cur = npc_db.cursor()

data = {
    "aarakocra":    ["none"],
    "aasimar":      ["thick", "wispy", "curly", "wavy", "wiry", "oily", "lush", "poofy", "braided", "shiny", "parted", "slicked back", "pulled back"],
    "bugbear":      ["none"],
    "dragonborn":   ["none"],
    "drow":         ["thick", "wispy", "curly", "wavy", "wiry", "oily", "lush", "poofy", "braided", "shiny", "parted", "slicked back", "pulled back"],
    "dwarf":        ["thick", "wispy", "curly", "wavy", "wiry", "oily", "lush", "poofy", "braided", "shiny", "parted", "slicked back", "pulled back"],
    "elf":          ["thick", "wispy", "curly", "wavy", "wiry", "oily", "lush", "poofy", "braided", "shiny", "parted", "slicked back", "pulled back"],
    "firlbolg":     ["thick", "wispy", "curly", "wavy", "wiry", "oily", "lush", "poofy", "braided", "shiny", "parted", "slicked back", "pulled back"],  
    "genasi":       ["thick", "wispy", "curly", "wavy", "wiry", "oily", "lush", "poofy", "braided", "shiny", "parted", "slicked back", "pulled back"],
    "gith":         ["thick", "wispy", "curly", "wavy", "wiry", "oily", "lush", "poofy", "braided", "shiny", "parted", "slicked back", "pulled back"],
    "goblin":       ["thick", "wispy", "curly", "wavy", "wiry", "oily", "lush", "poofy", "braided", "shiny", "parted", "slicked back", "pulled back" ],
    "goliath":      ["thick", "wispy", "curly", "wavy", "wiry", "oily", "lush", "poofy", "braided", "shiny", "parted", "slicked back", "pulled back"],
    "gnome":        ["thick", "wispy", "curly", "wavy", "wiry", "oily", "lush", "poofy", "braided", "shiny", "parted", "slicked back", "pulled back"],
    "half-elf":     ["thick", "wispy", "curly", "wavy", "wiry", "oily", "lush", "poofy", "braided", "shiny", "parted", "slicked back", "pulled back"],  
    "halfling":     ["thick", "wispy", "curly", "wavy", "wiry", "oily", "lush", "poofy", "braided", "shiny", "parted", "slicked back", "pulled back"],  
    "half-orc":     ["thick", "wispy", "curly", "wavy", "wiry", "oily", "lush", "poofy", "braided", "shiny", "parted", "slicked back", "pulled back"],
    "hobgoblin":    ["thick", "wispy", "curly", "wavy", "wiry", "oily", "lush", "poofy", "braided", "shiny", "parted", "slicked back", "pulled back"],   
    "human":        ["thick", "wispy", "curly", "wavy", "wiry", "oily", "lush", "poofy", "braided", "shiny", "parted", "slicked back", "pulled back"],
    "kenku":        ["none"],
    "kobold":       ["none"],
    "lizardfolk":   ["none"],
    "orc":          ["thick", "wispy", "curly", "wavy", "wiry", "oily", "lush", "poofy", "braided", "shiny", "parted", "slicked back", "pulled back"],
    "tabaxi":       ["none"],
    "tiefling":     ["thick", "wispy", "curly", "wavy", "wiry", "oily", "lush", "poofy", "braided", "shiny", "parted", "slicked back", "pulled back"],  
    "tortle":       ["none"],
    "triton":       ["thick", "wispy", "curly", "wavy", "wiry", "oily", "lush", "poofy", "braided", "shiny", "parted", "slicked back", "pulled back"]
}

for race in data:
    for hair_type in data[race]:
        cur.execute("INSERT INTO npc_hair_type (race, hair_type) VALUES (?, ?);", (race, hair_type))
        npc_db.commit()