import sqlite3

npc_db = sqlite3.connect("../npc_db")
cur = npc_db.cursor()

face_shapes = [
    "square",
    "round",
    "oblong",
    "oval",
    "elongated",
    "narrow",
    "heart-shaped",
    "chiseled",
    "sculpted",
    "craggy",
    "soft",
    "strong"
]

for face_shape in face_shapes:
    cur.execute("INSERT INTO npc_face_shape (face_shape) VALUES (?);", (face_shape,))
    npc_db.commit()