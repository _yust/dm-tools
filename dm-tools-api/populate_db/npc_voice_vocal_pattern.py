import sqlite3

npc_db = sqlite3.connect("../npc_db")
cur = npc_db.cursor()

vocal_patterns = [
    "incoherent except for a few words",
    "stutters",
    "lots of 'um'",
    "lots of 'like'",
    "lots of swearing",
    "uses 'thee' and 'thou'",
    "never stops to breathe",
    "short sentences",
    "speaks in third person",
    "doesn't conjugate well (me make good soup)",
    "all S sounds become Z sounds",
    "all W sounds become V sounds",
    "Rs are always rolled",
    "whiny",
    "speaks through clenched teeth, always",
    "barely opens mouth to speak",
    "all TH sounds become Z sounds",
    "Repeats the last word of a sentence twice",
    "Repeats adjectives (The sky is blue-blue-blue!)",
    "Repeats every question asked before answering",
    "Emphasises the wrong sylabbles",
    "Pauses very often",
    "Speaks out of the corner of the mouth",
    "Soft letters are elongated",
    "Slurs",
    "Sighs after every sentence",
    "Never uses any form of the verb 'are/to be' (I very smart. You not smart.)"
]

for vocal_pattern in vocal_patterns:
    cur.execute("INSERT INTO npc_voice_vocal_pattern (vocal_pattern) VALUES (?);", (vocal_pattern,))
    npc_db.commit()